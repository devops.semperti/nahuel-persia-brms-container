FROM jbossdemocentral/developer:jdk8-uid

MAINTAINER Nahuel Persia

# Environment Variables
ENV JBOSS_HOME_PATH /opt/jboss
ENV JBOSS_EAP jboss-eap-6.4.0-installer.jar
ENV JBOSS_EAP_PATCH jboss-eap-6.4.7-patch.zip
ENV JBOSS_BRMS jboss-brms-6.3.0.GA-installer.jar



RUN curl "http://rhel-repositorio.semperti.local/npersia/jboss/eap/patch/jboss-eap-6.4.7-patch.zip" >> $JBOSS_HOME_PATH/$JBOSS_EAP_PATCH

RUN curl "http://rhel-repositorio.semperti.local/npersia/jboss/eap/installer/jboss-eap-6.4.0-installer.jar" >> $JBOSS_HOME_PATH/$JBOSS_EAP

RUN curl "http://rhel-repositorio.semperti.local/npersia/jboss/brms/installer/jboss-brms-6.3.0.GA-installer.jar" >> $JBOSS_HOME_PATH/$JBOSS_BRMS

#COPY installs/$JBOSS_EAP installs/$JBOSS_EAP_PATCH installs/$JBOSS_BRMS $JBOSS_HOME_PATH/


COPY support/installation-brms support/installation-eap support/installation-brms.variables support/installation-eap.variables $JBOSS_HOME_PATH/




USER root
RUN chown 1000:1000 $JBOSS_HOME_PATH/$JBOSS_BRMS $JBOSS_HOME_PATH/$JBOSS_EAP
USER 1000

RUN java -jar $JBOSS_HOME_PATH/$JBOSS_EAP $JBOSS_HOME_PATH/installation-eap -variablefile $JBOSS_HOME_PATH/installation-eap.variables
RUN $JBOSS_HOME_PATH/target/jboss-eap-6.4/bin/jboss-cli.sh --command="patch apply $JBOSS_HOME_PATH/$JBOSS_EAP_PATCH --override-all"

RUN java -jar $JBOSS_HOME_PATH/$JBOSS_BRMS $JBOSS_HOME_PATH/installation-brms -variablefile $JBOSS_HOME_PATH/installation-brms.variables

RUN rm /opt/jboss/target/jboss-eap-6.4/standalone/configuration/standalone.xml
COPY support/standalone.xml /opt/jboss/target/jboss-eap-6.4/standalone/configuration/


RUN rm /opt/jboss/target/jboss-eap-6.4/standalone/configuration/application-roles.properties
COPY support/application-roles.properties /opt/jboss/target/jboss-eap-6.4/standalone/configuration/

RUN rm /opt/jboss/target/jboss-eap-6.4/standalone/configuration/application-users.properties
COPY support/application-users.properties /opt/jboss/target/jboss-eap-6.4/standalone/configuration/




USER root
RUN chown 1000:1000 -R $JBOSS_HOME_PATH/target/jboss-eap-6.4/standalone/log/
RUN chmod o+rw -R $JBOSS_HOME_PATH/target/jboss-eap-6.4/standalone/log
RUN chmod o+rw -R $JBOSS_HOME_PATH/target/jboss-eap-6.4/standalone/data/content
RUN ln -s /opt/jboss /home/jboss
#USER 1000



RUN chmod o+rwX -R $JBOSS_HOME_PATH/target/jboss-eap-6.4/

# Run as JBoss
USER 1000

# Expose Ports
EXPOSE 9990 9999 8080

VOLUME /opt /home

# Run BRMS
CMD ["/opt/jboss/target/jboss-eap-6.4/bin/standalone.sh","-c","standalone.xml","-b", "0.0.0.0","-bmanagement","0.0.0.0"]

