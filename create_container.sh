PWD=$(pwd)

JBOSS=$PWD/jboss
HOME=$PWD/home

#-v $JBOSS:/opt \
#-v $HOME:/home \
docker create \
    --name=$1 \
    -v /tmp/ejemplo_docker/brms-container/home:/home \
    -p 8080:8080 \
    -p 9999:9999 \
    -p 9990:9990 \
    npersia/brms:1.0
