SRC_DIR=./installs
EAP=jboss-eap-6.4.0-installer.jar
EAP_PATCH=jboss-eap-6.4.7-patch.zip
SUPPORT_DIR=./support
JBOSS_HOME=./target/jboss-eap-6.4
DIR_PATCH=echo pwd
BPMS=jboss-bpmsuite-6.3.0.GA-installer.jar
BRMS=jboss-brms-6.3.0.GA-installer.jar



echo "instalar eap"
java -jar $SRC_DIR/$EAP $SUPPORT_DIR/installation-eap -variablefile $SUPPORT_DIR/installation-eap.variables
echo "parchear eap"
$JBOSS_HOME/bin/jboss-cli.sh --command="patch apply $SRC_DIR/$EAP_PATCH --override-all"
echo "instalar brms"
java -jar $SRC_DIR/$BRMS $SUPPORT_DIR/installation-brms -variablefile $SUPPORT_DIR/installation-brms.variables
